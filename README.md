devops-tasks
===

- Все роли разработанны для ОС Debian 9, в качестве теста использовался Debian 9.9
- В файле tasks можно прочитать само тестовое задание и мои пометки того, что(по моему мнению) я выполнил а что нет


Запуск Ansible playbooks
------------
Создайте файл инветаризации, например:

````
# test
localhost
````

###Запуск playbooks:
**ПРИМЕЧАНИЕ** во всех плейбуках идет установка сервисов а потом настройка, 
установку можно закоментировать если сервисы уже установлены на хосте. 
Роли по установки сервисов называются одноименно nginx, apache, mysql, php

#### Настройка сервисов на сервере

*  Задание по настройке Nginx:
````bash
ansible-playbook -i test nginx.yml
````
*  Задание по настройке Apache:
````bash
ansible-playbook -i test apache.yml
````
*  Задание по настройке PHP:
````bash
ansible-playbook -i test php.yml
````
*  Задание по настройке MySQL:
````bash
ansible-playbook -i test mysql.yml
````

#### Настройка площадок на сервере

*  Задание по настройке площадки для работы сайта site.ru:
````bash
ansible-playbook -i test site.ru.yml
````
* Задание по настройке площадки для работы сайта site-2.ru:
````bash
ansible-playbook -i test site-2.ru.yml
````
*  Задание по настройке phpMyAdmin как алиас:
````bash
ansible-playbook -i test pma.yml
````
